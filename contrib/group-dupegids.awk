#!/usr/bin/awk -f
#
# Remove groups that have duplicate gids. The first group wins.
BEGIN{ FS=":" }
{
    if ($3 in gids) {
        print "duplicate gid="$3" name="$1", ignoring" > "/dev/stderr";
        next;
    }
    gids[$3]=$0;
    print $0;
}
