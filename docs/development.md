Profiling
---------

Prepare `profile.data`:

```
zig build -Drelease-small=true && \
    perf record --call-graph=dwarf \
        zig-out/bin/turbonss-unix2db --passwd passwd --group group
```

Perf interactive:

```
perf report -i perf.data
```

Flame graph:

```
perf script | inferno-collapse-perf | inferno-flamegraph > profile.svg
```

For v2
------

These are desired for the next DB format:
- Compress strings with fsst.
- Trim first 4 bytes from the cmph headers.

Dependencies
------------

This project uses [git subtrac][git-subtrac] for managing dependencies. They
work just like regular submodules, except all the refs of the submodules are in
this repository. Repeat after me: all the submodules are in this repository.
So if you have a copy of this repo, dependencies will not disappear.

[git-subtrac]: https://apenwarr.ca/log/20191109
