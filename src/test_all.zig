test "turbonss test suite" {
    _ = @import("bdz.zig");
    _ = @import("cmph.zig");
    _ = @import("compress.zig");
    _ = @import("Corpus.zig");
    _ = @import("DB.zig");
    _ = @import("ErrCtx.zig");
    _ = @import("Group.zig");
    _ = @import("header.zig");
    _ = @import("libnss.zig");
    _ = @import("PackedGroup.zig");
    _ = @import("PackedUser.zig");
    _ = @import("shell.zig");
    _ = @import("User.zig");
    _ = @import("validate.zig");

    // main
    _ = @import("turbonss-getent.zig");
    _ = @import("turbonss-unix2db.zig");
    _ = @import("turbonss-analyze.zig");
    _ = @import("turbonss-makecorpus.zig");
}
